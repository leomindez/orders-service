# Getting Started

### Architecture
<a>![architecture-diagram](/art/purchase-ecommerce.png)</a>

Use microservices to separate the domain business logic. 
It could be helpful to have different components with a different platform/language/technology.

Those microservices are going to be registered and called with a service registry. 
I suggest using [CONSUL](https://www.consul.io). Consul helps us to retrieve the information with just one entry point. The replication is not a stopper with the consul and it is going to distribute all the requests between containers. 

The service registry is going to be called by Backend for Front-End ([BFF](https://docs.microsoft.com/en-us/azure/architecture/patterns/backends-for-frontends)) gateways. 
I decided to use BFF to use the right services for each client. 

For example, if we need to send push notification only for mobile clients, 
we are not required to use the notifications service in the same gateway for the desktop client. 

Those BFFs could be developed by using GraphQL or another technology that allows complex queries.


### Order Service
I decided to implement the order service. I used Spring Boot Web Flux version with Kotlin Programming Language. 
I used coroutines to handle the async tasks. For example, request items from the database. I proposed to use MongoDB as a database service.
Handle the information with a schemaless database that allows generating complex queries or replicate information for different purposes. 
For example, I was thinking about the tracking order. If a client wants to track the purchase. We can update the tracking information like the position, latitude, longitude, etc. 

### DB Diagram
<a>![db-diagram](/art/orders-service.png)</a>

# [DEMO](https://orders.ngrok.io/webjars/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#/)
I am using ngrok to expose the service, but it is a piece of cake to deploy it in AWS or GCP. 
I am also using [JIB](https://github.com/GoogleContainerTools/jib) to create the docker build. Jib is an incredible tool to generate java containers.

### Tools
* Spring Boot Web-Flux 
* Kotlin 
* Coroutines
* MongoDB
* Docker
* Docker-Compose
* Google JIB 

### Run locally
```
./gradlew build && ./gradlew jibDockerBuild && docker-compose up -d
```

### Reference Documentation

For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.2/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.2/gradle-plugin/reference/html/#build-image)
* [Coroutines section of the Spring Framework Documentation](https://docs.spring.io/spring/docs/5.3.3/spring-framework-reference/languages.html#coroutines)
* [Spring Data MongoDB](https://docs.spring.io/spring-boot/docs/2.4.2/reference/htmlsingle/#boot-features-mongodb)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/2.4.2/reference/htmlsingle/#using-boot-devtools)

### Guides

The following guides illustrate how to use some features concretely:

* [Accessing Data with MongoDB](https://spring.io/guides/gs/accessing-data-mongodb/)

### Additional Links

These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)

