package com.leo.walmart.ecommerce.orders.service

import com.leo.walmart.ecommerce.orders.model.Order
import com.leo.walmart.ecommerce.orders.model.OrderStatus
import com.leo.walmart.ecommerce.orders.model.Product
import com.leo.walmart.ecommerce.orders.model.User
import com.leo.walmart.ecommerce.orders.repository.OrderRepository
import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import java.util.*

@ExtendWith(MockKExtension::class)
internal class OrderServiceImplTest {

    @MockK(relaxed = true)
    lateinit var orderRepository: OrderRepository

    @MockK(relaxed = true)
    private lateinit var user: User

    private lateinit var orderService: OrderService

    @BeforeEach
    fun setUp() {
        orderService = OrderServiceImpl(orderRepository)
    }

    @AfterEach
    fun cleanUp() {
        clearAllMocks()
    }

    @Test
    fun `should create a new order`() = runBlocking {
        val order = Order(UUID.randomUUID(), user, listOf<Product>(), OrderStatus.PENDING)
        coEvery { orderRepository.save(any()) } returns order
        val newOrder = orderService.createOrder(order)
        assertNotNull(order)
        assertEquals(order.id, newOrder.id)
    }

    @Test
    fun `should get order by id`() = runBlocking {
        val orderId = UUID.randomUUID()
        val order = Order(orderId, user, listOf<Product>(), OrderStatus.PENDING)
        coEvery { orderRepository.findById(any()) } returns Optional.of(order)
        val savedOrder = orderService.getOrderById(orderId)
        assertNotNull(savedOrder)
        assertEquals(orderId, savedOrder.id)
    }

    @Test
    fun `should return a list of orders`() = runBlocking {
        val orderList = listOf(Order(UUID.randomUUID(), user, listOf<Product>(), OrderStatus.PENDING))
        coEvery { orderRepository.findAll() } returns orderList
        val orders = orderService.getOrders()
        assertNotNull(orders)
    }
}