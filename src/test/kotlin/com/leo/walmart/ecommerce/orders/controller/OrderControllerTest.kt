package com.leo.walmart.ecommerce.orders.controller

import com.leo.walmart.ecommerce.orders.model.Address
import com.leo.walmart.ecommerce.orders.model.AddressType
import com.leo.walmart.ecommerce.orders.model.Order
import com.leo.walmart.ecommerce.orders.model.OrderStatus
import com.leo.walmart.ecommerce.orders.model.Product
import com.leo.walmart.ecommerce.orders.model.User
import com.leo.walmart.ecommerce.orders.service.OrderService
import io.mockk.junit5.MockKExtension
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import java.util.*
import kotlin.NoSuchElementException

@WebFluxTest(OrderController::class)
@ExtendWith(MockKExtension::class)
@AutoConfigureWebTestClient
internal class OrderControllerTest {

    @Autowired
    lateinit var webTestClient: WebTestClient

    @MockBean
    lateinit var orderService: OrderService
    lateinit var order: Order

    @BeforeEach
    fun setUp(){
        val user = User(
            fullName = "Leonel Mendez Jimenez", addresses = listOf<Address>(
                Address(
                    type = AddressType.SHIPPING_ADDRESS,
                    street = "prolongacion presa",
                    neighborhood = "Lomas",
                    externalNumber = "104",
                    complement = "B",
                    zipCode = "11400",
                    municipality = "Miguel Hidalgo",
                    state = "CDMX",
                    city = "CDMX"
                )
            )
        )
        val products = listOf(
            Product(
                "Entertainment",
                "Nintendo Switch",
                "consola de video juegos",
                20_000.00,
                "VDG-199283884",
                listOf()
            )
        )

        order = Order(user = user, products = products, status = OrderStatus.IN_PROGRESS)

    }

    @Test
    fun `should create a new order`(): Unit = runBlocking {
        webTestClient
            .post()
            .uri("/v1/orders")
            .bodyValue(order)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isCreated
    }

    @Test
    fun `should return 400 bad request response`(): Unit = runBlocking {
        webTestClient
            .post()
            .uri("/v1/orders")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isBadRequest
    }

    @Test
    fun `should get an order by id`(): Unit = runBlocking {
        Mockito.`when`(orderService.getOrderById(UUID(1L,1L))).thenReturn(order)
        webTestClient
            .get()
            .uri("/v1/orders/9c78cba9-d6b5-4c2b-84c1-09bc33564754")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk
    }


    @Test
    fun `should return 404 response when order is not found`(): Unit = runBlocking {
        Mockito.`when`(orderService.getOrderById(UUID.fromString("9c78cba9-d6b5-4c2b-84c1-09bc33564754"))).thenThrow(NoSuchElementException::class.java)
        webTestClient
            .get()
            .uri("/v1/orders/9c78cba9-d6b5-4c2b-84c1-09bc33564754")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound
    }

    @Test
    fun `should return list of orders`(): Unit = runBlocking {
        Mockito.`when`(orderService.getOrders()).thenReturn(listOf())
        webTestClient
            .get()
            .uri("/v1/orders")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk
    }
}