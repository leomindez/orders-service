package com.leo.walmart.ecommerce.orders.model

import java.time.Instant
import java.util.*

data class Order(
    val id: UUID? = UUID.randomUUID(),
    val user: User,
    val products: List<Product>,
    val status: OrderStatus,
    var total: Double? = 0.0 ,
    val createdAt: Instant = Instant.now(),
    val updatedAt: Instant = Instant.now()
)
