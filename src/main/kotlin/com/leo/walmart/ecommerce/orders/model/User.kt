package com.leo.walmart.ecommerce.orders.model

import java.time.Instant
import java.util.*

data class User(
    val id: UUID = UUID.randomUUID(),
    val fullName: String,
    val addresses: List<Address>,
    val createdAt: Instant = Instant.now(),
    val updatedAt: Instant? = Instant.now()
)
