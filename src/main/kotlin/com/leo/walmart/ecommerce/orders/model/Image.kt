package com.leo.walmart.ecommerce.orders.model

import java.util.*

data class Image(
    val id: UUID = UUID.randomUUID(),
    val thumbnail_url: String,
    val full_url: String
)
