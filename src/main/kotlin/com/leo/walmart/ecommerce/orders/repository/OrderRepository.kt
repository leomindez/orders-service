package com.leo.walmart.ecommerce.orders.repository

import com.leo.walmart.ecommerce.orders.model.Order
import org.springframework.data.mongodb.repository.MongoRepository
import java.util.*

interface OrderRepository : MongoRepository<Order, UUID>