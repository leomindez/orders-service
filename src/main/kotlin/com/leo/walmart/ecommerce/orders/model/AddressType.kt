package com.leo.walmart.ecommerce.orders.model

enum class AddressType {
    SHIPPING_ADDRESS,
    BILLING_ADDRESS
}
