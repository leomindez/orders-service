package com.leo.walmart.ecommerce.orders.controller

import com.leo.walmart.ecommerce.orders.model.Order
import com.leo.walmart.ecommerce.orders.service.OrderService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping("/v1/orders")
@CrossOrigin
class OrderController(private val orderService: OrderService) {

    @PostMapping
    suspend fun create(@RequestBody order: Order): ResponseEntity<Order> {
        val createdOrder = orderService.createOrder(order)
        return ResponseEntity.status(HttpStatus.CREATED).body(createdOrder)
    }

    @GetMapping("/{id}")
    suspend fun getById(@PathVariable("id") orderId: UUID): ResponseEntity<Order> {
        val order = orderService.getOrderById(orderId)
        return ResponseEntity.ok(order)
    }

    @GetMapping
    suspend fun getAll(): ResponseEntity<List<Order>> {
        val orders = orderService.getOrders()
        return ResponseEntity.ok(orders)
    }
}