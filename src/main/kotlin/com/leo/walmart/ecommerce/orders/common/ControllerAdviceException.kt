package com.leo.walmart.ecommerce.orders.common

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestController

@ControllerAdvice(annotations = [RestController::class])
class ControllerAdviceException {

    @ExceptionHandler(value = [NoSuchElementException::class])
    fun handlerNotFound(e: Exception): ResponseEntity<Unit>? {
        println(e)
        return ResponseEntity.notFound().build()
    }

    @ExceptionHandler(value = [Exception::class])
    fun handler(e: Exception): ResponseEntity<Unit>? {
        println(e)
        return ResponseEntity.badRequest().build()
    }
}