package com.leo.walmart.ecommerce.orders.model

import java.time.Instant

data class Address(
    val type: AddressType,
    val street: String,
    val neighborhood: String,
    val externalNumber: String,
    val complement: String,
    val zipCode: String,
    val municipality: String,
    val state: String,
    val city: String,
    val createdAt: Instant = Instant.now(),
    val updatedAt: Instant? = Instant.now()
)
