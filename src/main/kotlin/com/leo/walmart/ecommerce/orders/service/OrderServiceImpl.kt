package com.leo.walmart.ecommerce.orders.service

import com.leo.walmart.ecommerce.orders.model.Order
import com.leo.walmart.ecommerce.orders.repository.OrderRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class OrderServiceImpl(private val orderRepository: OrderRepository) : OrderService {

    override suspend fun createOrder(order: Order): Order = runCatching {
        val order = order
        order.total = order.products.sumByDouble { it.price }
        orderRepository.save(order)
    }.getOrThrow()

    override suspend fun getOrderById(orderId: UUID): Order = runCatching {
        orderRepository.findById(orderId).get()
    }.getOrThrow()

    override suspend fun getOrders(): List<Order> = runCatching {
        orderRepository.findAll()
    }.getOrThrow()
}