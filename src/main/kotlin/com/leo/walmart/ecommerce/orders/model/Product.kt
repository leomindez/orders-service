package com.leo.walmart.ecommerce.orders.model

data class Product(
    val category: String,
    val name: String,
    val description: String,
    val price: Double,
    val sku: String,
    val images: List<Image>
)
