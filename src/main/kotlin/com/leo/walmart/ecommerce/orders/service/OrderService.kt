package com.leo.walmart.ecommerce.orders.service

import com.leo.walmart.ecommerce.orders.model.Order
import java.util.*

interface OrderService {
    suspend fun createOrder(order: Order): Order
    suspend fun getOrderById(orderId: UUID): Order
    suspend fun getOrders(): List<Order>
}