package com.leo.walmart.ecommerce.orders.model

enum class OrderStatus {
    PENDING,
    IN_PROGRESS,
    CANCELLED,
    DELIVERED
}